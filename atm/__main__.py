# Test program by running "python3 -m atm" from the cis206-atm-project folder.

from account import account
from Card import card as card_library
from CardReader import card_reader as card_reader_library
from log import log as log_library
from Receipt import receipt as receipt_library
from Network import network as network_library


def test_account():
    try:
        print("Testing Account")
        account_details = account.Account(4938337167696407)
        if account_details.test():
            print("All tests successful!")
    except Exception as exception:
        print(exception)


def test_card():
    try:
        print("Testing Card")
        card = card_library.Card.from_database("5574842517140315")
        print(card)
    except Exception as exception:
        print(exception)


def test_card_reader():
    try:
        print("Testing CardReader")
        card = card_reader_library.CardReader.read_card
        print(card)
    except Exception as exception:
        print(exception)
        


def test_log():
    try:
        print("Testing log")
        log = log_library.Log()
        log.test()
    except Exception as exception:
        print(exception)


def test_network():
    try:
        print("Testing Network")
        network = network_library.Network()
        network.test()
    except Exception as exception:
        print(exception)


def test_receipt():
    try:
        print("Testing Receipt")
        receipt = receipt_library.Receipt()
        receipt.test()
    except Exception as exception:
        print(exception)


if __name__ == "__main__":
    test_account()
    test_card()
    test_log()
    test_network()
    test_receipt()

