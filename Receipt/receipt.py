"""
Name: Myca Bautista
Class: CIS 206
Subject: Modules and Classes | ATM Project - Receipt

Examples:


References:
http://www.math-cs.gordon.edu/courses/cs211/ATMExample/javadoc/banking/Receipt.html
https://docs.python.org/3/library/enum.html
http://hplgit.github.io/primer.html/doc/pub/class/._class-readable002.html
https://stackoverflow.com/questions/727761/python-str-and-lists

"""
import datetime


class Receipt:
    """Encapsulate information to be printed on a receipt."""

    def __init__(self, account_number=None, transaction=None, balance=None):
        """Creates the receipt object

        Args:

        Raises:
        """

        self._account_number = account_number
        self._transaction = transaction
        self._balance = balance

    def __str__(self):
        """Send information to printer """

        return(
            "========== CIS206 BANK RECEIPT ============\n"
            f"Account Number: {self.account_number}",
            f"Transaction Type: {self._transaction}",
            f"Amount: {self._amount}",
            f"Balance: {self._balance}"
        )

    @property
    def account_number(self):
        """Gets bank account that was used

        -card number

        """
        return self._account_number

    @account_number.setter
    def account_number(self, value):
        """Sets the bank account number

        """
        assert isinstance(value, int)
        self._account_number = int(value)

    @property
    def transaction(self):
        """Gets the transaction
        -Inquiry
        -Transfer
        -Withdraw
        -Deposit

        **** receive it from account
        :return:
        """
        return self._transaction

    @transaction.setter
    def transaction(self, transaction_type):
        """Sets the transaction

        :return:
        """
        assert isinstance(transaction_type, str)
        self._transaction = str(transaction_type)

    @property
    def balance(self):
        """Gets the balance
        -available balance

        :return:
        """
        return self.balance

    @balance.setter
    def balance(self, value):
        """Sets the balance

        :return:
        """
        assert isinstance(value, float)
        self._balance = float(value)

    def get_full_account_details(self, account_number, amount, balance, transaction_type):
        """Gets full account details from account

        """


    def get_lines(self):
        """Gets the individual lines to be printed
        """
        print(str(datetime.datetime.now()))

    def ready_for_print(self):
        """Gets information from account to print

        """

    def test(self):
        """Tests all functions"""

        receipt = Receipt()

        return True


if __name__ == "__main__":
    """Run tests if module is executed by name."""
    receipt = Receipt()
    if receipt.test():
        print("All tests successful!")
