"""
Name: Tyler Alesse
Class: CIS 206
Subject: Modules and Classes | ATM Project - Receipt

Examples:


References:
http://www.sqlitetutorial.net/sqlite-foreign-key/
http://www.sqlitetutorial.net/sqlite-insert/
https://en.wikiversity.org/wiki/Applied_Programming/Databases
https://en.wikiversity.org/wiki/Applied_Programming/Modules_and_Classes
https://en.wikiversity.org/wiki/Applied_Programming/Modules_and_Classes/Python3
https://en.wikiversity.org/wiki/Python_Programming/Classes
https://en.wikiversity.org/wiki/Python_Programming/Modules
https://stackoverflow.com/questions/11753871/getting-the-type-of-a-column-in-sqlite
https://stackoverflow.com/questions/1601151/how-do-i-check-in-sqlite-whether-a-table-exists
https://stackoverflow.com/questions/25220985/updating-rows-in-a-sqlite-database-using-python
https://stackoverflow.com/questions/28126140/python-sqlite3-operationalerror-no-such-table/28126276#28126276
https://stackoverflow.com/questions/30698566/sqlite3-select-statement-fails-with-parameters-are-of-unsupported-type
https://stackoverflow.com/questions/4291165/sqlite-copy-data-from-one-table-to-another
https://stackoverflow.com/questions/48209010/sql-operational-error-no-such-table
https://stackoverflow.com/questions/628637/best-data-type-for-storing-currency-values-in-a-mysql-database
https://www.python-course.eu/python3_properties.php
https://www.sqlite.org/doclist.html
https://www.sqlite.org/foreignkeys.html
https://www.w3schools.com/sql/sql_foreignkey.asp
https://www.w3schools.com/sql/sql_insert.asp


"""

import sqlite3
import sys


class Account:
    """This module contains the details and methods
    normally associated with a bank account"""

    def __init__(self, account_number=None):
        if type(account_number) is not int:
            raise ValueError(f"Account Number is not of type integer.\nReceived: {account_number}")

        self._account_number = account_number
        self._connection = sqlite3.connect("Data/atminfo.db")
        self._cursor = self._connection.cursor()
        self._cursor.execute("SELECT * FROM AccountInformation WHERE AccountNumber=?;", [self._account_number])
        data = self._cursor.fetchone()
        self._card_number = data[0]
        self._account_type = data[2]
        self._balance = data[3]

    @property
    def card_number(self):
        """Gets account number

        :param self:
        :return:
        """
        return self._card_number

    @property
    def account_number(self):
        """Gets account balance

        :param self:
        :return:
        """
        return self._account_number

    @property
    def account_type(self):
        """Gets account balance

        :param self:
        :return:
        """
        return self._account_type

    @property
    def balance(self):
        """Sets account balance

        """
        return self._balance

    @balance.setter
    def balance(self, value):
        """Sets account balance

        """
        self._balance = value

    @property
    def connection(self):
        """Gets the connection contect

        """
        return self._connection

    @property
    def cursor(self):
        """Gets the cursor contect

        """
        return self._cursor

    def transaction_details(self, amount, transferred_to, transaction_type="Unknown"):
        return {
            "Transaction": transaction_type,
            "Card Number": self.card_number,
            "Account Number": self.account_number,
            "Account Type": self.account_type,
            "Amount": amount,
            "Transferred To": transferred_to,
            "New Balance": self.balance,
        }

    def inquiry(self):
        """Gets the balance from your account

        """
        self.cursor.execute("SELECT Balance FROM AccountInformation WHERE AccountNumber=?", [self._account_number])
        self.balance(self.cursor.fetchone()[0])
        return self.transaction_details(None, None, "Inquiry")

    def withdraw(self, amount):
        """Withdraws money from your account

        """
        if amount > self.balance:
            raise ValueError(f"Cannot withdraw more than you own."
                             f"Amount in Account:  ${self.balance}"
                             f"Amount to Withdraw: ${amount}")
        self.balance(self.balance - amount)
        self.cursor.execute("UPDATE AccountInformation Balance=? WHERE AccountNumber=?",
                            [self.balance, self.account_number])
        return self.transaction_details(amount, None, "Withdrawal")

    def deposit(self, amount):
        """Deposits money into your account

        """
        self.balance(self.balance + amount)
        self.cursor.execute("UPDATE AccountInformation Balance=? WHERE AccountNumber=?",
                             [self.balance, self.account_number])
        return self.transaction_details(amount, None, "Deposit")

    def transfer(self, amount, to_account_number):
        """Transfers money from your account to another account

        """
        if amount > self.balance:
            raise ValueError(f"Cannot withdraw more than you own."
                             f"Amount in Account:  ${self.balance}"
                             f"Amount to Withdraw: ${amount}")
        if len(to_account_number) != 6:
            raise ValueError(f"Account Number is not of length 6.\nReceived: {to_account_number}")
        self.withdraw(amount)
        Account(to_account_number).deposit(amount)
        return self.transaction_details(amount, to_account_number, "Transfer")

    def test(self):
        """Tests all functions"""
        if self.account_number != 4938337167696407:
            raise Exception(f"Account Number did not match the database account number.\nReceived {self.account_number}")
        if self.account_type != "Savings":
            raise Exception(f"Account Type did not match the database account type.\nReceived {self.account_type}")
        if self.card_number != 5372076886117588:
            raise Exception(f"Card Number did not match the database card number.\nReceived {self.card_number}")
        if self.balance != 10000:
            raise Exception(f"Account Balance did not match the database balance.\nReceived {self.balance}")

        return True


if __name__ == "__main__":
    """Run tests if module is executed by name."""
    account_details = Account(4938337167696407)
    if account_details.test():
        print("All tests successful!")
