from Card import card as card_library
import sqlite3


class CardReader:
	
	number = "5574842517140315"
	status = "empty"
	
	def __init__(self, status):
	   """initializes class reader.
	   Args:
	        status: the status of the card reader
	   """
	   self._status = status
	
	@property
	def set_status(self, status):
		_status = self._status
		return status
	
	@classmethod	
	def read_card(self, number):
		self.read_card(number)
		card = card_library.Card.from_database(number)
		print(card)
		return card
	
	def test_card_reader_one(self):
		number = "5574842517140315"
		card = card_library.Card.from_database(number)
		print("test one complete")


if __name__ == "__main__":
        status = "full"
        CardReader.set_status
        print(CardReader.status)
        CardReader.read_card("5574842517140315")