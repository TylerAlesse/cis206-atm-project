"""
log Module - Karol Cenda

==============================   Import   ==============================

from log.log import log

============================== Usage Ex 1 ==============================

log(log_msg, log_level, log_filename)

============================== Usage Ex 2 ==============================

log = log()

log.level = log_level
log.msg = log_msg
log.filename = log_filename
log.log()

========================================================================

References:
    https://stackoverflow.com/questions/13699283/how-to-get-the-callers-filename-method-name-in-python
    https://www.cl.cam.ac.uk/~mgk25/iso-time.html
    https://tecadmin.net/get-current-date-time-python/

"""

from datetime import datetime
import inspect
import os


class Log:
    """Logs level and message to log.txt."""

    def __init__(self, log_msg=None, log_level=None, log_filename=None):
        """Creates log object.

        Args:
            log_msg (str): Optional log Message
            log_level (str): Optional log Level

        """
        self._log_msg = log_msg
        self._log_level = log_level
        self._caller = None
        self._datetime = None
        self._log_string = None
        self._log_filename = None
        self._path_type = None

        frame = inspect.stack()[1]
        module = inspect.getmodule(frame[0])
        caller_full_path = module.__file__

        self._caller_full_path = caller_full_path

        if log_filename is not None:
            self._log_filename = log_filename
        else:
            self._log_filename = "log.txt"

        if log_msg is not None and log_level is not None:
            self.log()
        elif log_msg is None and log_level is None:
            pass
        else:
            raise TypeError("log() takes 2 positional arguments but 1 was given. "
                            "\nERROR: Please specify both log_msg and log_level OR set the values outside of the class "
                            "initialization!")

    @property
    def msg(self):
        """Gets log message.

        Returns:
            str: log message

        """
        return self._log_msg

    @msg.setter
    def msg(self, log):
        """Sets log message.

        Args:
            log (str): log message

        """
        assert isinstance(log, str)
        self._log_msg = str(log)

    @property
    def level(self):
        """Gets log level.

        Returns:
            str: log level

        """
        return self._log_level

    @level.setter
    def level(self, level):
        """Sets log level.

        Args:
            level (str): log level

        """
        assert isinstance(level, str)
        self._log_level = str(level)

    @property
    def filename(self):
        """Gets log filename.

        Returns:
            str: log filename

        """
        return self._log_filename

    @filename.setter
    def filename(self, filename):
        """Sets log filename.

        Args:
            filename (str): log filename

        """
        assert isinstance(filename, str)
        self._log_filename = str(filename)

    def get_caller_from_stack(self):
        """Gets name of file which initialized the class.

        Args:
            None

        Returns:
            None

        """
        file_index_forward_slash = self._caller_full_path.rfind("/") + 1
        file_index_backward_slash = self._caller_full_path.rfind("\\") + 12

        if file_index_forward_slash > file_index_backward_slash:
            self._path_type = "/"
            file_index = file_index_forward_slash
        elif file_index_backward_slash > file_index_forward_slash:
            self._path_type = "\\"
            file_index = file_index_backward_slash
        else:
            file_index = 0

        self._caller = self._caller_full_path[file_index:]

    def get_current_datetime(self):
        """Gets current date and time.

        Args:
            None

        Returns:
            None

        """
        self._datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    def build_log_string(self):
        """Builds string for logging with formatting.

        Args:
            None

        Returns:
            None

        """
        self._log_string = "[" + self._datetime + "] " \
                     + self._caller + ": " \
                     + "[" + self._log_level + "] - " \
                     + self._log_msg

    def write_to_log(self):
        """Writes log_string to log.txt.

        Args:
            None

        Returns:
            None

        """
        current_path = os.path.dirname(__file__)
        log_path = os.path.join(current_path, ".." + self._path_type + "Data")
        with open(log_path + self._path_type + self._log_filename, "a") as log:
            log.write(self._log_string)
            log.write("\n")

    def log(self):
        """Runs class main functions.

        Args:
            None

        Raises:
            ValueError: If log_msg and/or log_level are not initialized!

        """

        if self._log_msg is None:
            raise ValueError("You must specify a log message!")
        if self._log_level is None:
            raise ValueError("You must specify a log level!")

        self.get_caller_from_stack()
        self.get_current_datetime()
        self.build_log_string()
        self.write_to_log()

    def clear(self, log_filename=None):
        """Clears (Deletes and Recreates) log file.

        Args:
            log_filename: Optional file name

        """

        if log_filename is not None:
            self._log_filename = log_filename

        file_index_forward_slash = self._caller_full_path.rfind("/") + 1
        file_index_backward_slash = self._caller_full_path.rfind("\\") + 1

        if file_index_forward_slash > file_index_backward_slash:
            self._path_type = "/"
        elif file_index_backward_slash > file_index_forward_slash:
            self._path_type = "\\"

        current_path = os.path.dirname(__file__)
        full_log_path = current_path + self._path_type + self._log_filename
        os.remove(full_log_path)

        with open(full_log_path, "a") as x:
            pass

    def test_one(self):
        """Tests module's functionality.

        Args:
            None

        Raises:
            ValueError: If one log produces more than one line inside log file!

        """
        test_log_filename = "test_log.txt"

        test_log_msg = "Something went wrong!"
        test_log_level = "ERROR"
        test_log = Log(log_msg=test_log_msg, log_level=test_log_level, log_filename=test_log_filename)
        test_datetime = test_log._datetime
        test_caller = test_log._caller
        test_log_string = "[" + test_datetime + "] " + test_caller + ": [" + test_log_level + "] - " + test_log_msg

        assert test_log._log_string == test_log_string

        self.get_caller_from_stack()
        current_path = os.path.dirname(__file__)
        log_path = os.path.join(current_path, ".." + self._path_type + "Data")

        test_log_data = []
        with open(log_path + self._path_type + test_log_filename, "r") as log:
            for line in log:
                line = line.strip()
                test_log_data.append(line)

        if len(test_log_data) > 1:
            raise ValueError("One log write has produced multiple lines!")

        assert test_log_string == test_log_data[0]

        os.remove(log_path + self._path_type + test_log_filename)

    def test_two(self):
        """Tests module's functionality.

        Args:
            None

        Raises:
            ValueError: If one log produces more than one line inside log file!

        """
        test_log_filename = "test_log.txt"

        test_log_msg = "Everything worked!"
        test_log_level = "INFO"
        test_log = Log(log_msg=test_log_msg, log_level=test_log_level, log_filename=test_log_filename)
        test_datetime = test_log._datetime
        test_caller = test_log._caller
        test_log_string = "[" + test_datetime + "] " + test_caller + ": [" + test_log_level + "] - " + test_log_msg

        assert test_log._log_string == test_log_string

        self.get_caller_from_stack()
        current_path = os.path.dirname(__file__)
        log_path = os.path.join(current_path, ".." + self._path_type + "Data")

        test_log_data = []
        with open(log_path + self._path_type + test_log_filename, "r") as log:
            for line in log:
                line = line.strip()
                test_log_data.append(line)

        if len(test_log_data) > 1:
            raise ValueError("One log write has produced multiple lines!")

        assert test_log_string == test_log_data[0]

        os.remove(log_path + self._path_type + test_log_filename)

    def test(self):
        """Tests module's functionality.

        Args:
            None

        """
        print("Starting log.py test...")

        self.test_one()
        print("Test 1: Success!")

        self.test_two()
        print("Test 2: Success!")

        return True


if __name__ == "__main__":
    """Run tests if module is executed by name."""
    log = Log()
    if log.test():
        print("All tests successful!")
