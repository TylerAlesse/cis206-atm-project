# Info:
    
    This logging module will create a text file called 'log.txt' unless otherwise specified. 
    This file will contain the global event log and will always be located inside the 'Log' directory.
    One line of logging information will include date/time, name of file which called the logging module, log level, and log message.
        
    Format:
        [yyyy-mm-dd hh:mm:ss] filename: [level] - message
        
    Level Standards:
        INFO: Simple information
        STATUS: Update on status
        ERROR: An error has occurred

# Importing:

    from log.log import Log

# Usage (Example 1 - Argument Passing):

    log_msg is REQUIRED
    log_level is REQUIRED
    log_filename is OPTIONAL (Default: log.txt)
    
#

    Usage:
        Log(log_msg, log_level, log_filename)
        
    Example:
        Log(log_msg="Something went wrong!", log_level="ERROR", log_filename="mylog.txt")

    mylog.txt:
        [yyyy-mm-dd hh:mm:ss] myfile.py: [ERROR] - Something went wrong!

# Usage (Example 2 - Setting Values):

    log_msg is REQUIRED
    log_level is REQUIRED
    log_filename is OPTIONAL (Default: log.txt)

#
    
    Usage:
       log = Log()
       
       log.level = log_level
       log.msg = log_msg
       log.filename = log_filename
       log.log()
      
    Example:
        log = Log()
        
        log.level = "ERROR"
        log.msg = "Something went wrong!"
        log.filename = "mylog.txt"
        log.log()

    mylog.txt:
        [yyyy-mm-dd hh:mm:ss] myfile.py: [ERROR] - Something went wrong!
        
# Clearing (Example):

    This module has a built in clear function. 
    It will delete and recreate the file called 'log.txt' unless otherwise specified.
    
    log_filename is OPTIONAL (Default: log.txt)
#
    Usage:
        log = Log()
        log.clear()
        
    Example:
        log = Log()
        log.clear(log_filename="mylog.txt")
    
    mylog.txt:
        *Empty*

# Testing:
    
    Simply run 'log.py' to test module.
    
    OR
    
    Use built in test function using 'log.test()'
