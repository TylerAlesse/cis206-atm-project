""" SMTP server connection function sending emails with pins to customers"""

import smtplib
import secrets
import sqlite3


DATABASE = "Data/atminfo.db"


class Network:
    """ Class for SMTP server connection

    """

    def __init__(self, card_number=None):
        self.card_number = card_number

    @staticmethod
    def get_data(card_number, g_email, g_name):
        """ Pulls customer's name and email form Database.

        :param card_number: customer's card number
        :param g_email: customer's email from Database
        :param g_name: customer's name from Database
        :return: name and email
        """

        try:
            conn = sqlite3.connect(DATABASE)
        except Exception as exception:
            print("Can't connect to %s." % DATABASE)
            print(exception)
            raise
        try:
            c = conn.cursor()
            if g_email is True:
                c.execute(
                    "SELECT CustomerEmail "
                    "FROM CustomerInformation "
                    "WHERE CardNumber = {}".format(card_number))
                emails = c.fetchone()
                for email in emails:
                    return email
            if g_name is True:
                c.execute(
                    "SELECT CustomerName "
                    "FROM CustomerInformation "
                    "WHERE CardNumber = {}".format(card_number))
                names = c.fetchone()
                for name in names:
                    return name
        except Exception as exception:
            print("Can't execute SELECT command.")
            print(exception)
        finally:
            conn.close()

    @staticmethod
    def pin_generator():
        """ Generates pins.

        :return: pin_code: (string) 4 characters hex.
        """
        pin_code = secrets.randbelow(9999)
        pin_number = str(pin_code)
        return pin_number

    @staticmethod
    def update_database(card_number, pin_number):
        """ Updates / Adds newly generated pin to Database

        :param card_number: users card number
        :param pin_number: pin generated for card number
        :return: None
        """

        try:
            conn = sqlite3.connect(DATABASE)
        except Exception as exception:
            print("Can't connect to %s." % DATABASE)
            print(exception)
            raise

        try:
            c = conn.cursor()
            c.execute("INSERT INTO PinCode (CardNumber, Pin_Code) "
                      "VALUES ({}, {})".format(card_number, pin_number))
            conn.commit()
        except Exception as exception:
            print("Can't execute INSERT command.")
            print(exception)

        finally:
            conn.close()

    @staticmethod
    def compare_pins(card_number, pin_entered):
        """ Compares pin generated and the one entered by user on keypad

        :param card_number: users card number
        :param pin_entered: pin entered by user
        :return: if pin entered matches the generated one, method returns
                account numbers for the card number,
                if pin entered doesn't match returns False.
        """

        try:
            conn = sqlite3.connect(DATABASE)
        except Exception as exception:
            print("Can't connect to %s." % DATABASE)
            print(exception)
            raise

        try:
            c = conn.cursor()
            c.execute("SELECT Pin_Code "
                      "FROM PinCode "
                      "WHERE CardNumber = {} "
                      "ORDER BY PinID DESC LIMIT 1".format(card_number))
            pin_to_compare = c.fetchone()
            for i in pin_to_compare:
                pin = str(i)

            pin = '123'  # Temporary pin - need rest of the program working
            # to remove this part.
            pin_entered = '123'  # Temporary pin - need rest of the program
            # working to remove this part.

            if pin == pin_entered:
                c = conn.cursor()
                c.execute(
                        "SELECT AccountNumber_1, "
                        "AccountNumber_2, "
                        "AccountNumber_3 "
                        "FROM AccountInformation "
                        "WHERE CardNumber = {}".format(card_number))
                account_numbers = c.fetchall()
                return account_numbers
            if pin != pin_entered:
                return False

        except Exception as exception:
            print("Can't execute INSERT command.")
            print(exception)

        finally:
            conn.close()

    def send_pin(self, card_number):
        """ Module triggers pin generator and new_smtp to send pin generated.

        :param card_number: card number of the customer
        :return: None
        """

        g_email = True
        g_name = False
        email = self.get_data(card_number, g_email, g_name)
        g_email = False
        g_name = True
        name = self.get_data(card_number, g_email, g_name)
        pin_number = self.pin_generator()
        self.update_database(card_number, pin_number)
        self.new_smtp(email, name, pin_number)

    @staticmethod
    def new_smtp(email, name, pin_number):
        """ Sends email with pin to customer.

        :param email: customer's email address
        :param name: customer's name
        :param pin_number: generated pin
        :return: None
        """

        gmail_user = 'cis206@robercik.com'
        gmail_password = '206206Cis'
        sent_from = gmail_user
        to = email
        subject = 'Important Message - ATM transaction Related'
        body = "Hi {}".format(name), "You Pin: {}".format(pin_number)

        email_text = """\
        From: %s
        To: %s
        Subject: %s

        %s
        """ % (sent_from, to, subject, body)

        try:
            server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
            server.ehlo()
            server.login(gmail_user, gmail_password)
            server.sendmail(sent_from, to, email_text)
            server.close()
            print('Email sent!')

        except:
            print('Server problem.')

    def test(self):
        self.test_send_pin()
        print(".Network Test passed Successfully.")
        self.test_pin_gen()
        print("Pin generated successfully.")
        self.test_get_data_email()
        print("Database Data successfully reached.")
        self.test_get_data_name()
        print("Database Data successfully reached.")
        print("\n*** All test Successful ***")

    def test_get_data_email(self):
        card_number = '5372071328831110'
        g_email = True
        g_name = False
        email = self.get_data(card_number, g_email, g_name)
        assert email == 'r_trebenda@mail.harpercollege.edu'
        print("Email returned:", email)

    def test_get_data_name(self):
        card_number = '5372071328831110'
        g_email = False
        g_name = True
        name = self.get_data(card_number, g_email, g_name)
        assert name == 'Robert Trebenda'
        print("Name returned:", name)

    def test_send_pin(self):
        card_number = '5372071328831110'
        self.send_pin(card_number)

    def test_pin_gen(self):
        pin_number = self.pin_generator()
        assert pin_number <= '9999'
        print("Pin generated:", pin_number)


if __name__ == "__main__":
    network = Network()
    if network.test():
        print("Test OK")
