import sys
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton
from PyQt5.QtCore import pyqtSlot


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Harper ATM'
        self.left = 10
        self.top = 10
        self.width = 420
        self.height = 250
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        button1 = QPushButton('Enter button', self)
        button1.setToolTip('This s an example button')
        button1.move(100, 70)
        button1.clicked.connect(self.on_click1)

        button2 = QPushButton('Cancel button', self)
        button2.setToolTip('This s an example button')
        button2.move(100, 120)
        button2.clicked.connect(self.on_click2)

        button3 = QPushButton('Clear button', self)
        button3.setToolTip('This s an example button')
        button3.move(100, 170)
        button3.clicked.connect(self.on_click3)

        self.show()

    @pyqtSlot()
    def on_click1(self):
        print('Enter button click')

    @pyqtSlot()
    def on_click2(self):
        print('Cancel button click')

    @pyqtSlot()
    def on_click3(self):
        print('Clear button click')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())