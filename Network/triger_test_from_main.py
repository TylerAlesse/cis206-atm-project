from Network import network
network = network.Network()


def main():

    card_number = 5372071328831110
    network.send_pin(card_number)
    
    pin_entered = 1234
    account_numbers = network.compare_pins(card_number, pin_entered)
    
    if account_numbers is False:
        print("Entered pin doesn't patch the generated one.")
    
    if account_numbers is not False:
        print("Pin was correct, account numbers for the card are:")
        print(account_numbers)


if __name__ == "__main__":
    main()
