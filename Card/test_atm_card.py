"""This program Create a program that:

References:
    * https://www.youtube.com/watch?v=wfcWRAxRVBA
    * https://www.youtube.com/watch?v=rq8cL2XMM5M
    * https://www.youtube.com/watch?v=jCzT9XFZ5bw&t=306s
    *
"""
import sqlite3
from card import Card


def test_one():

    input_selected_customer = 5574842517140315
    selected_customer = Card.from_database(input_selected_customer)

    expected_output = Card(cardnumber=5574842517140315, customerid=8,
                           customername="Nasim Naghibi",
                           customeremail="n_naghibimoham@mail.harpercollege.edu",
                           customeraddress="688 Coleman Road", zipcode=60042,
                           expirationdate="10-30-2025", cardcvv=575, cardname="Grumpy")

    assert selected_customer.customerid == expected_output.customerid
    assert selected_customer.cardnumber == expected_output.cardnumber
    assert selected_customer.customeremail == expected_output.customeremail
    assert selected_customer.customeraddress == expected_output.customeraddress
    assert selected_customer.customername == expected_output.customername
    assert selected_customer.zipcode == expected_output.zipcode
    assert selected_customer.expirationdate == expected_output.expirationdate
    assert selected_customer.cardcvv == expected_output.cardcvv
    assert selected_customer.cardname == expected_output.cardname


def test_two():

    input_selected_customer = 5372071328831110
    selected_customer = Card.from_database(input_selected_customer)

    expected_output = Card(cardnumber=5372071328831110, customerid=5,
                           customername="Robert Trebenda",
                           customeremail="r_trebenda@mail.harpercollege.edu",
                           customeraddress="90 Oakridge Alley", zipcode=60002,
                           expirationdate="02-28-2024", cardcvv=422, cardname="Dopey")

    assert selected_customer.customerid == expected_output.customerid
    assert selected_customer.cardnumber == expected_output.cardnumber
    assert selected_customer.customeremail == expected_output.customeremail
    assert selected_customer.customeraddress == expected_output.customeraddress
    assert selected_customer.customername == expected_output.customername
    assert selected_customer.zipcode == expected_output.zipcode
    assert selected_customer.expirationdate == expected_output.expirationdate
    assert selected_customer.cardcvv == expected_output.cardcvv
    assert selected_customer.cardname == expected_output.cardname
