"""This program Create a program that:
    creates ATM card and able to read any of them
    from the selected card.
References:
    * https://www.youtube.com/watch?v=wfcWRAxRVBA
    * https://www.youtube.com/watch?v=rq8cL2XMM5M
    * https://www.youtube.com/watch?v=jCzT9XFZ5bw&t=306s
"""
import sqlite3


class Card(object):

    def __init__(self,cardnumber, customerid=None, customername=None,
                 customeremail=None,customeraddress=None, zipcode=None,
                 expirationdate=None, cardcvv=None, cardname=None):
        """ Initializing the values of instance members for the new object."""

        self._customerid = customerid
        self._customername = customername
        self._customeremail = customeremail
        self._customeraddress = customeraddress
        self._zipcode = zipcode
        self._cardnumber = cardnumber
        self._expirationdate = expirationdate
        self._cardcvv = cardcvv
        self._cardname = cardname

    @property
    def customerid(self):
        return "{}".format(self._customerid)

    @customerid.setter
    def customerid(self,customerid):
        self._customerid = customerid

    @property
    def customername(self):
        return "{}".format(self._customername)

    @customername.setter
    def customername(self,customername):
        self._customername = customername

    @property
    def customeremail(self):
        return "{}@email.com".format(self._customeremail)

    @customeremail.setter
    def customeremail(self,customeremail):
        self._customeremail = customeremail

    @property
    def customeraddress(self):
        return "{}".format(self._customeraddress)

    @customeraddress.setter
    def customeraddress(self,customeraddress):
        self._customeraddress = customeraddress


    @property
    def zipcode(self):
        return "{}".format(self._zipcode)

    @zipcode.setter
    def zipcode(self,zipcode):
        self._zipcode = zipcode


    @property
    def cardnumber(self):
        return "{}".format(self._cardnumber)

    @cardnumber.setter
    def cardnumber(self,cardnumber):
        self._cardnumber = cardnumber

    def __str__(self):
        """Printing the new object."""

        return str(self._customerid) + ",  " + self._customername + ",  "\
            + self._customeremail + ",  " + self._customeraddress +\
            ",  " + str(self._zipcode) + ",  " + str(self._cardnumber) +\
            ",  " + str(self._expirationdate) + ",  " + str(self._cardcvv) + ",  " + self._cardname

    @property
    def expirationdate(self):
        return "{}".format(self._expirationdate)

    @expirationdate.setter
    def expirationdate(self,expirationdate):
        self._expirationdate = expirationdate

    @property
    def cardcvv(self):
        return "{}".format(self._cardcvv)

    @cardcvv.setter
    def cardcvv(self,cardcvv):
        self._cardcvv = cardcvv

    @property
    def cardname(self):
        return "{}".format(self._cardname)

    @cardname.setter
    def cardname(self,cardname):
        self._cardname = cardname

    @staticmethod
    def from_database(cardnumber):
        """getting the selected customer card from
        database "atminfo.db". """

        connect_1 = sqlite3.connect("Data/atminfo.db")
        # connect_1 = sqlite3.connect("../Data/atminfo.db")
        cursor_db = connect_1.cursor()
        cursor_db.execute("SELECT * FROM CustomerInformation WHERE cardnumber={};".format(cardnumber))

        selected_customer = cursor_db.fetchone()
        customer_card = Card(customerid=selected_customer[0],
                             customername=selected_customer[1],
                             customeremail=selected_customer[2],
                             customeraddress=selected_customer[3],
                             zipcode=selected_customer[4],
                             cardnumber=selected_customer[5],
                             expirationdate=selected_customer[6],
                             cardcvv=selected_customer[7],
                             cardname=selected_customer[8])

        return customer_card


def main():
    """Runs the main program logic."""

    new_card = Card.from_database(5574842517140315)

    print(new_card)


if __name__ == "__main__":

    main()
